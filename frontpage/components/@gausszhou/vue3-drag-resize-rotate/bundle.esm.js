import { openBlock as F, createElementBlock as $, normalizeStyle as j, normalizeClass as J, Fragment as at, renderList as rt, withModifiers as G, renderSlot as K } from "vue";
const Q = /* @__PURE__ */ new WeakMap();
function lt(t) {
  let e = Q.get(t);
  return e || Q.set(t, e = /* @__PURE__ */ Object.create(null)), e;
}
function z(t, e, ...i) {
  t && t.$emit && t.$emit(e, ...i);
  const s = lt(t)[e];
  return s && s.map((o) => o.apply(t, i)), t;
}
function Z(t) {
  return typeof t == "function" || Object.prototype.toString.call(t) === "[object Function]";
}
function X(t, e, i, s = 1) {
  const o = Math.round(e / s / t[0]) * t[0], n = Math.round(i / s / t[1]) * t[1];
  return [o, n];
}
function tt(t, e, i) {
  return t - e - i;
}
function et(t, e, i) {
  return t - e - i;
}
function R(t, e, i) {
  return e !== null && t < e ? e : i !== null && i < t ? i : t;
}
function Y(t, e, i, s, o) {
  const n = Math.PI / 180 * o, l = Math.cos(n), u = Math.sin(n);
  let h = i - t, r = s - e;
  return {
    x: h * l - r * u + t,
    y: h * u + r * l + e
  };
}
function ct(t, e) {
  let i = Math.atan2(e, t);
  return i = Math.round(180 / Math.PI * i), i < 0 && (i = 360 + i), i;
}
function it(t, e, i) {
  let s = t;
  const o = [
    "matches",
    "webkitMatchesSelector",
    "mozMatchesSelector",
    "msMatchesSelector",
    "oMatchesSelector"
  ].find((n) => Z(s[n]));
  if (!Z(s[o]))
    return !1;
  do {
    if (s[o](e))
      return !0;
    if (s === i)
      return !1;
    s = s.parentNode;
  } while (s);
  return !1;
}
function ut(t) {
  const e = window.getComputedStyle(t);
  return [
    parseFloat(e.getPropertyValue("width"), 10),
    parseFloat(e.getPropertyValue("height"), 10)
  ];
}
function A(t, e, i) {
  t && (t.attachEvent ? t.attachEvent("on" + e, i) : t.addEventListener ? t.addEventListener(e, i, !0) : t["on" + e] = i);
}
function C(t, e, i) {
  t && (t.detachEvent ? t.detachEvent("on" + e, i) : t.removeEventListener ? t.removeEventListener(e, i, !0) : t["on" + e] = null);
}
const dt = (t, e) => {
  const i = t.__vccOpts || t;
  for (const [s, o] of e)
    i[s] = o;
  return i;
}, D = {
  mouse: {
    start: "mousedown",
    move: "mousemove",
    stop: "mouseup"
  },
  touch: {
    start: "touchstart",
    move: "touchmove",
    stop: "touchend"
  }
}, mt = {
  userSelect: "none",
  MozUserSelect: "none",
  WebkitUserSelect: "none",
  MsUserSelect: "none"
}, pt = {
  userSelect: "auto",
  MozUserSelect: "auto",
  WebkitUserSelect: "auto",
  MsUserSelect: "auto"
};
let k = D.mouse;
const ft = {
  name: "VueDragResizeRotate",
  props: {
    className: {
      type: String,
      default: "vue-drag-resize-rotate"
    },
    classNameDraggable: {
      type: String,
      default: "draggable"
    },
    classNameResizable: {
      type: String,
      default: "resizable"
    },
    // 新增开启旋转时的自定义类名
    classNameRotatable: {
      type: String,
      default: "rotatable"
    },
    classNameDragging: {
      type: String,
      default: "dragging"
    },
    classNameResizing: {
      type: String,
      default: "resizing"
    },
    // 新增组件处于旋转时的自定义类名
    classNameRotating: {
      type: String,
      default: "rotating"
    },
    classNameActive: {
      type: String,
      default: "active"
    },
    classNameHandle: {
      type: String,
      default: "handle"
    },
    disableUserSelect: {
      type: Boolean,
      default: !0
    },
    enableNativeDrag: {
      type: Boolean,
      default: !1
    },
    preventDeactivation: {
      type: Boolean,
      default: !1
    },
    active: {
      type: Boolean,
      default: !1
    },
    draggable: {
      type: Boolean,
      default: !0
    },
    resizable: {
      type: Boolean,
      default: !0
    },
    // 新增 旋转 默认为false 不开启
    rotatable: {
      type: Boolean,
      default: !1
    },
    // 锁定宽高比
    lockAspectRatio: {
      type: Boolean,
      default: !1
    },
    // 新增 外部传入纵横比 w/h
    outsideAspectRatio: {
      type: [Number, String],
      default: 0
    },
    w: {
      type: [Number, String],
      default: 200,
      validator: (t) => typeof t == "number" ? t > 0 : t === "auto"
    },
    h: {
      type: [Number, String],
      default: 200,
      validator: (t) => typeof t == "number" ? t > 0 : t === "auto"
    },
    minWidth: {
      type: Number,
      default: 0,
      validator: (t) => t >= 0
    },
    minHeight: {
      type: Number,
      default: 0,
      validator: (t) => t >= 0
    },
    maxWidth: {
      type: Number,
      default: 1 / 0,
      validator: (t) => t >= 0
    },
    maxHeight: {
      type: Number,
      default: 1 / 0,
      validator: (t) => t >= 0
    },
    x: {
      type: [String, Number],
      default: 0
    },
    y: {
      type: [String, Number],
      default: 0
    },
    z: {
      type: [String, Number],
      default: "auto",
      validator: (t) => typeof t == "string" ? t === "auto" : t >= 0
    },
    // 新增 初始旋转角度
    r: {
      type: [String, Number],
      default: 0
    },
    // 新增 旋转手柄 rot
    handles: {
      type: Array,
      default: () => ["tl", "tm", "tr", "mr", "br", "bm", "bl", "ml", "rot"],
      validator: (t) => {
        const e = /* @__PURE__ */ new Set(["tl", "tm", "tr", "mr", "br", "bm", "bl", "ml", "rot"]);
        return new Set(t.filter((i) => e.has(i))).size === t.length;
      }
    },
    dragHandle: {
      type: String,
      default: null
    },
    dragCancel: {
      type: String,
      default: null
    },
    axis: {
      type: String,
      default: "both",
      validator: (t) => ["x", "y", "both"].includes(t)
    },
    grid: {
      type: Array,
      default: () => [1, 1]
    },
    parent: {
      type: [Boolean, String],
      default: !1
    },
    onDragStart: {
      type: Function,
      default: () => !0
    },
    onDrag: {
      type: Function,
      default: () => !0
    },
    onResizeStart: {
      type: Function,
      default: () => !0
    },
    onResize: {
      type: Function,
      default: () => !0
    },
    // 新增 回调事件
    onRotateStart: {
      type: Function,
      default: () => !0
    },
    onRotate: {
      type: Function,
      default: () => !0
    },
    // 冲突检测
    isConflictCheck: {
      type: Boolean,
      default: !1
    },
    // 元素对齐
    snap: {
      type: Boolean,
      default: !1
    },
    // 新增 是否对齐容器边界
    snapBorder: {
      type: Boolean,
      default: !1
    },
    // 当调用对齐时，用来设置组件与组件之间的对齐距离，以像素为单位
    snapTolerance: {
      type: Number,
      default: 5,
      validator: function(t) {
        return typeof t == "number";
      }
    },
    // 缩放比例
    scaleRatio: {
      type: Number,
      default: 1,
      validator: (t) => typeof t == "number"
    },
    // handle是否缩放
    handleInfo: {
      type: Object,
      default: () => ({
        size: 8,
        offset: -4,
        switch: !0
      })
    }
  },
  data: function() {
    return {
      left: this.x,
      top: this.y,
      right: null,
      bottom: null,
      // 新增旋转角度
      rotate: this.r,
      width: null,
      height: null,
      widthTouched: !1,
      heightTouched: !1,
      // 纵横比变量
      aspectFactor: null,
      // 容器的大小
      parentWidth: null,
      parentHeight: null,
      // 设置最小和最大尺寸
      minW: this.minWidth,
      minH: this.minHeight,
      maxW: this.maxWidth,
      maxH: this.maxHeight,
      // 定义控制手柄
      handle: null,
      enabled: this.active,
      resizing: !1,
      dragging: !1,
      // 新增 表明组件是否正处于旋转状态
      rotating: !1,
      zIndex: this.z,
      // 新增 保存中心点位置，用于计算旋转的方向矢量
      lastCenterX: 0,
      lastCenterY: 0,
      // 父元素左上角的坐标值
      parentX: 0,
      parentY: 0
    };
  },
  computed: {
    handleStyle() {
      return (t) => {
        if (!this.handleInfo.switch)
          return { display: this.enabled ? "block" : "none" };
        if (t === "rot" && !this.rotatable)
          return { display: "none" };
        if (t !== "rot" && !this.resizable)
          return { display: "none" };
        const e = (this.handleInfo.size / this.scaleRatio).toFixed(2), i = (this.handleInfo.offset / this.scaleRatio).toFixed(2), s = (e / 2).toFixed(2), o = {
          tl: {
            top: `${i}px`,
            left: `${i}px`
          },
          tm: {
            top: `${i}px`,
            left: `calc(50% - ${s}px)`
          },
          tr: {
            top: `${i}px`,
            right: `${i}px`
          },
          mr: {
            top: `calc(50% - ${s}px)`,
            right: `${i}px`
          },
          br: {
            bottom: `${i}px`,
            right: `${i}px`
          },
          bm: {
            bottom: `${i}px`,
            right: `calc(50% - ${s}px)`
          },
          bl: {
            bottom: `${i}px`,
            left: `${i}px`
          },
          ml: {
            top: `calc(50% - ${s}px)`,
            left: `${i}px`
          },
          rot: {
            top: `-${e * 3}px`,
            left: "50%"
          }
        }, n = {
          width: o[t].width || `${e}px`,
          height: o[t].height || `${e}px`,
          top: o[t].top,
          left: o[t].left,
          right: o[t].right,
          bottom: o[t].bottom
        }, l = {
          tl: 0,
          tm: 1,
          tr: 2,
          mr: 3,
          br: 4,
          bm: 5,
          bl: 6,
          ml: 7,
          rot: 8
        };
        if (t !== "rot") {
          const u = [
            "nw-resize",
            "n-resize",
            "ne-resize",
            "e-resize",
            "se-resize",
            "s-resize",
            "sw-resize",
            "w-resize"
          ], r = this.rotate + 22.5, d = Math.floor(r / 45);
          let p = (l[t] + d) % 8;
          n.cursor = u[p];
        }
        return n.display = this.enabled ? "block" : "none", n;
      };
    },
    style() {
      return {
        transform: `translate(${this.left}px, ${this.top}px) rotate(${this.rotate}deg)`,
        width: this.computedWidth,
        height: this.computedHeight,
        zIndex: this.zIndex,
        fontSize: this.handleInfo.size * 2 + "px",
        ...this.dragging && this.disableUserSelect ? mt : pt
      };
    },
    // 控制柄显示与否
    actualHandles() {
      return !this.resizable && !this.rotatable ? [] : this.handles;
    },
    //  根据left right 算出元素的宽度
    computedWidth() {
      return this.w === "auto" && !this.widthTouched ? "auto" : this.width + "px";
    },
    // 根据top bottom 算出元素的宽度
    computedHeight() {
      return this.h === "auto" && !this.heightTouched ? "auto" : this.height + "px";
    }
  },
  watch: {
    active(t) {
      this.enabled = t, t ? (this.updateParentSize(), z(this, "activated")) : z(this, "deactivated");
    },
    x(t) {
      this.resizing || this.dragging || (this.parent && (this.bounds = this.calcDragLimits()), this.moveHorizontally(t));
    },
    y(t) {
      this.resizing || this.dragging || (this.parent && (this.bounds = this.calcDragLimits()), this.moveVertically(t));
    },
    z(t) {
      (t >= 0 || t === "auto") && (this.zIndex = t);
    },
    // 新增 监听外部传入参数  旋转角度
    r(t) {
      t >= 0 && (this.rotate = t % 360);
    },
    // 锁定纵横比
    lockAspectRatio(t) {
      t ? this.outsideAspectRatio ? this.aspectFactor = this.outsideAspectRatio : this.aspectFactor = this.width / this.height : this.aspectFactor = void 0;
    },
    // 自定义纵横比
    outsideAspectRatio(t) {
      t && (this.aspectFactor = t);
    },
    minWidth(t) {
      t > 0 && t <= this.width && (this.minW = t);
    },
    minHeight(t) {
      t > 0 && t <= this.height && (this.minH = t);
    },
    maxWidth(t) {
      this.maxW = t;
    },
    maxHeight(t) {
      this.maxH = t;
    },
    w(t) {
      this.resizing || this.dragging || (this.parent && (this.bounds = this.calcResizeLimits()), this.changeWidth(t));
    },
    h(t) {
      this.resizing || this.dragging || (this.parent && (this.bounds = this.calcResizeLimits()), this.changeHeight(t));
    }
  },
  created: function() {
    this.maxWidth && this.minWidth > this.maxWidth && console.warn("[Vdr warn]: Invalid prop: minWidth cannot be greater than maxWidth"), this.maxWidth && this.minHeight > this.maxHeight && console.warn("[Vdr warn]: Invalid prop: minHeight cannot be greater than maxHeight"), this.elmX = 0, this.elmY = 0, this.elmW = 0, this.elmH = 0, this.lastCenterX = 0, this.lastCenterY = 0, this.fixedXName = "", this.fixedYName = "", this.fixedX = 0, this.fixedY = 0, this.TL = {}, this.TR = {}, this.BL = {}, this.BR = {}, this.resetBoundsAndMouseState();
  },
  mounted: function() {
    this.enableNativeDrag || (this.$el.ondragstart = () => !1);
    const [t, e] = this.getParentSize();
    this.parentWidth = t, this.parentHeight = e;
    const [i, s] = ut(this.$el);
    this.aspectFactor = (this.w !== "auto" ? this.w : i) / (this.h !== "auto" ? this.h : s), this.outsideAspectRatio && (this.aspectFactor = this.outsideAspectRatio), this.width = this.w !== "auto" ? this.w : i, this.height = this.h !== "auto" ? this.h : s, this.right = this.parentWidth - this.width - this.left, this.bottom = this.parentHeight - this.height - this.top, this.settingAttribute(), A(document.documentElement, "mousedown", this.deselect), A(document.documentElement, "touchend touchcancel", this.deselect), A(window, "resize", this.checkParentSize);
  },
  beforeUnmount: function() {
    C(document.documentElement, "mousedown", this.deselect), C(document.documentElement, "touchstart", this.handleUp), C(document.documentElement, "mousemove", this.move), C(document.documentElement, "touchmove", this.move), C(document.documentElement, "mouseup", this.handleUp), C(document.documentElement, "touchend touchcancel", this.deselect), C(window, "resize", this.checkParentSize);
  },
  methods: {
    // 重置边界和鼠标状态
    resetBoundsAndMouseState() {
      this.mouseClickPosition = {
        mouseX: 0,
        mouseY: 0,
        x: 0,
        y: 0,
        w: 0,
        h: 0
      }, this.bounds = {
        minLeft: null,
        maxLeft: null,
        minRight: null,
        maxRight: null,
        minTop: null,
        maxTop: null,
        minBottom: null,
        maxBottom: null
      };
    },
    // 检查父元素大小
    checkParentSize() {
      if (this.parent) {
        const [t, e] = this.getParentSize();
        this.right = t - this.width - this.left, this.bottom = e - this.height - this.top, this.parentWidth = t, this.parentHeight = e;
      }
    },
    // 更新获取父元素宽高
    updateParentSize() {
      const [t, e] = this.getParentSize();
      this.parentWidth = t, this.parentHeight = e;
    },
    // 获取父元素大小
    getParentSize() {
      if (this.parent === !0) {
        const t = window.getComputedStyle(this.$el.parentNode, null), e = this.$el.parentNode.getBoundingClientRect();
        return this.parentX = e.x, this.parentY = e.y, [
          Math.round(parseFloat(t.getPropertyValue("width"), 10)),
          Math.round(parseFloat(t.getPropertyValue("height"), 10))
        ];
      }
      if (typeof this.parent == "string") {
        const t = document.querySelector(this.parent);
        if (!(t instanceof HTMLElement))
          throw new Error(`The selector ${this.parent} does not match any element`);
        return [t.offsetWidth, t.offsetHeight];
      }
      return [null, null];
    },
    // 元素触摸按下
    elementTouchDown(t) {
      k = D.touch, this.elementDown(t);
    },
    elementMouseDown(t) {
      k = D.mouse, this.elementDown(t);
    },
    // 元素按下
    elementDown(t) {
      if (t instanceof MouseEvent && t.which !== 1)
        return;
      const e = t.target || t.srcElement;
      if (this.$el.contains(e)) {
        if (this.onDragStart(t) === !1)
          return;
        if (this.dragHandle && !it(e, this.dragHandle, this.$el) || this.dragCancel && it(e, this.dragCancel, this.$el)) {
          this.dragging = !1;
          return;
        }
        this.enabled || (this.enabled = !0, z(this, "activated"), z(this, "update:active", !0)), this.draggable && (this.dragging = !0), this.mouseClickPosition.mouseX = t.touches ? t.touches[0].pageX : t.pageX, this.mouseClickPosition.mouseY = t.touches ? t.touches[0].pageY : t.pageY, this.mouseClickPosition.left = this.left, this.mouseClickPosition.right = this.right, this.mouseClickPosition.top = this.top, this.mouseClickPosition.bottom = this.bottom, this.mouseClickPosition.width = this.width, this.mouseClickPosition.height = this.height, this.parent && (this.bounds = this.calcDragLimits()), A(document.documentElement, k.move, this.move), A(document.documentElement, k.stop, this.handleUp);
      }
    },
    // 计算移动范围
    calcDragLimits() {
      return this.rotatable ? {
        minLeft: -this.width / 2,
        maxLeft: this.parentWidth - this.width / 2,
        minRight: this.width / 2,
        maxRight: this.parentWidth + this.width / 2,
        minTop: -this.height / 2,
        maxTop: this.parentHeight - this.height / 2,
        minBottom: this.height / 2,
        maxBottom: this.parentHeight + this.height / 2
      } : {
        minLeft: this.left % this.grid[0],
        maxLeft: Math.floor((this.parentWidth - this.width - this.left) / this.grid[0]) * this.grid[0] + this.left,
        minRight: this.right % this.grid[0],
        maxRight: Math.floor((this.parentWidth - this.width - this.right) / this.grid[0]) * this.grid[0] + this.right,
        minTop: this.top % this.grid[1],
        maxTop: Math.floor((this.parentHeight - this.height - this.top) / this.grid[1]) * this.grid[1] + this.top,
        minBottom: this.bottom % this.grid[1],
        maxBottom: Math.floor((this.parentHeight - this.height - this.bottom) / this.grid[1]) * this.grid[1] + this.bottom
      };
    },
    // 取消选择
    deselect(t) {
      const e = t.target || t.srcElement, i = new RegExp(this.className + "-([trmbl]{2})", "");
      !this.$el.contains(e) && !i.test(e.className) && (this.enabled && !this.preventDeactivation && (this.enabled = !1, z(this, "deactivated"), z(this, "update:active", !1)), C(document.documentElement, k.move, this.move)), this.resetBoundsAndMouseState();
    },
    // 控制柄触摸按下
    handleTouchDown(t, e) {
      k = D.touch, this.handleDown(t, e);
    },
    // 控制柄按下
    handleDown(t, e) {
      if (e instanceof MouseEvent && e.which !== 1 || this.onResizeStart(t, e) === !1)
        return !1;
      e.stopPropagation && e.stopPropagation(), this.handle = t, this.handle === "rot" ? this.rotating = !0 : this.resizing = !0;
      let { top: i, left: s, width: o, height: n } = this.$el.getBoundingClientRect();
      this.lastCenterX = window.pageXOffset + s + o / 2, this.lastCenterY = window.pageYOffset + i + n / 2;
      let l = this.left, u = this.top, h = this.width, r = this.height, d = l + h / 2, p = u + r / 2, a = this.rotate;
      this.TL = Y(d, p, l, u, a), this.TR = Y(d, p, l + h, u, a), this.BL = Y(d, p, l, u + r, a), this.BR = Y(d, p, l + h, u + r, a), this.mouseClickPosition.mouseX = e.touches ? e.touches[0].pageX : e.pageX, this.mouseClickPosition.mouseY = e.touches ? e.touches[0].pageY : e.pageY, this.mouseClickPosition.left = this.left, this.mouseClickPosition.right = this.right, this.mouseClickPosition.top = this.top, this.mouseClickPosition.bottom = this.bottom, this.mouseClickPosition.width = this.width, this.mouseClickPosition.height = this.height, this.bounds = this.calcResizeLimits(), A(document.documentElement, k.move, this.move), A(document.documentElement, k.stop, this.handleUp);
    },
    // 计算调整大小范围
    calcResizeLimits() {
      let t = this.minW, e = this.minH, i = this.maxW, s = this.maxH;
      const [o, n] = this.grid, l = this.width, u = this.height, h = this.left, r = this.top, d = this.right, p = this.bottom;
      i = i - i % o, s = s - s % n;
      const a = {
        minLeft: null,
        maxLeft: null,
        minTop: null,
        maxTop: null,
        minRight: null,
        maxRight: null,
        minBottom: null,
        maxBottom: null
      };
      return this.parent ? (a.minLeft = h, a.maxLeft = h + Math.floor((l - t) / o), a.minTop = r, a.maxTop = r + Math.floor((u - e) / n), a.minRight = d, a.maxRight = d + Math.floor((l - t) / o), a.minBottom = p, a.maxBottom = p + Math.floor((u - e) / n), i && (a.minLeft = Math.max(a.minLeft, this.parentWidth - d - i), a.minRight = Math.max(a.minRight, this.parentWidth - h - i)), s && (a.minTop = Math.max(a.minTop, this.parentHeight - p - s), a.minBottom = Math.max(a.minBottom, this.parentHeight - r - s))) : (a.minLeft = null, a.maxLeft = h + Math.floor(l - t), a.minTop = null, a.maxTop = r + Math.floor(u - e), a.minRight = null, a.maxRight = d + Math.floor(l - t), a.minBottom = null, a.maxBottom = p + Math.floor(u - e), i && (a.minLeft = -(d + i), a.minRight = -(h + i)), s && (a.minTop = -(p + s), a.minBottom = -(r + s)), this.lockAspectRatio && i && s && (a.minLeft = Math.min(a.minLeft, -(d + i)), a.minTop = Math.min(a.minTop, -(s + p)), a.minRight = Math.min(a.minRight, -h - i), a.minBottom = Math.min(a.minBottom, -r - s))), a;
    },
    // 移动
    move(t) {
      this.resizing ? this.handleResize(t) : this.dragging ? this.handleDrag(t) : this.rotating && this.handleRotate(t);
    },
    // 获取鼠标或者触摸点的坐标
    getMouseCoordinate(t) {
      return t.type.indexOf("touch") !== -1 ? {
        x: t.changedTouches[0].clientX,
        y: t.changedTouches[0].clientY
      } : {
        x: t.pageX || t.clientX + document.documentElement.scrollLeft,
        y: t.pageY || t.clientY + document.documentElement.scrollTop
      };
    },
    handleRotate(t) {
      const { x: e, y: i } = this.getMouseCoordinate(t), s = e - this.lastCenterX, o = i - this.lastCenterY;
      this.rotate = (ct(s, o) + 90) % 360, z(this, "rotating", this.rotate);
    },
    // 元素移动
    async handleDrag(t) {
      const e = this.axis, i = this.grid, s = this.bounds, o = this.mouseClickPosition, n = e && e !== "y" ? o.mouseX - (t.touches ? t.touches[0].pageX : t.pageX) : 0, l = e && e !== "x" ? o.mouseY - (t.touches ? t.touches[0].pageY : t.pageY) : 0, [u, h] = X(i, n, l, this.scaleRatio), r = R(o.left - u, s.minLeft, s.maxLeft), d = R(o.top - h, s.minTop, s.maxTop);
      if (this.onDrag(r, d) === !1)
        return;
      const p = R(o.right + u, s.minRight, s.maxRight), a = R(o.bottom + h, s.minBottom, s.maxBottom);
      this.left = r, this.top = d, this.right = p, this.bottom = a, await this.snapCheck(), z(this, "dragging", this.left, this.top);
    },
    // 外部传参改动x
    moveHorizontally(t) {
      const [e] = X(this.grid, t, this.top, this.scale), i = R(e, this.bounds.minLeft, this.bounds.maxLeft);
      this.left = i, this.right = this.parentWidth - this.width - i;
    },
    // 外部传参改动y
    moveVertically(t) {
      const [, e] = X(this.grid, this.left, t, this.scale), i = R(e, this.bounds.minTop, this.bounds.maxTop);
      this.top = i, this.bottom = this.parentHeight - this.height - i;
    },
    // 控制柄移动
    handleResize(t) {
      const e = this.handle, i = this.scaleRatio, { TL: s, TR: o, BL: n, BR: l } = this;
      let { x: u, y: h } = this.getMouseCoordinate(t);
      !this.rotatable && this.parent && (u = R(u, this.parentX, this.parentX + this.parentWidth * i), h = R(h, this.parentY, this.parentY + this.parentHeight * i));
      let r = u - this.mouseClickPosition.mouseX, d = h - this.mouseClickPosition.mouseY;
      r = r / i, d = d / i;
      let p, a, x, W, T, b, w, H, M, c = {}, f = {}, g = {}, y = {}, m = {}, N = {}, v = {}, S = {};
      if (e.includes("m")) {
        switch (e) {
          case "tm":
            p = r + (s.x + o.x) / 2, a = d + (s.y + o.y) / 2, c = n, f = s, g = l, y = { x: p - c.x, y: a - c.y }, m = { x: f.x - c.x, y: f.y - c.y }, x = (y.x * m.x + y.y * m.y) / (Math.pow(m.x, 2) + Math.pow(m.y, 2)), v = { x: g.x - c.x, y: g.y - c.y }, S = { x: m.x * x, y: m.y * x };
            break;
          case "bm":
            p = r + (n.x + l.x) / 2, a = d + (n.y + l.y) / 2, c = s, f = n, g = o, y = { x: p - c.x, y: a - c.y }, m = { x: f.x - c.x, y: f.y - c.y }, x = (y.x * m.x + y.y * m.y) / (Math.pow(m.x, 2) + Math.pow(m.y, 2)), v = { x: g.x - c.x, y: g.y - c.y }, S = { x: m.x * x, y: m.y * x };
            break;
          case "ml":
            p = r + (s.x + n.x) / 2, a = d + (s.y + n.y) / 2, c = l, f = n, g = o, y = { x: p - c.x, y: a - c.y }, m = { x: f.x - c.x, y: f.y - c.y }, x = (y.x * m.x + y.y * m.y) / (Math.pow(m.x, 2) + Math.pow(m.y, 2)), S = { x: g.x - c.x, y: g.y - c.y }, v = { x: m.x * x, y: m.y * x };
            break;
          case "mr":
            p = r + (o.x + o.x) / 2, a = d + (o.y + o.y) / 2, c = n, f = l, g = s, y = { x: p - c.x, y: a - c.y }, m = { x: f.x - c.x, y: f.y - c.y }, x = (y.x * m.x + y.y * m.y) / (Math.pow(m.x, 2) + Math.pow(m.y, 2)), S = { x: g.x - c.x, y: g.y - c.y }, v = { x: m.x * x, y: m.y * x };
            break;
        }
        b = c.x + (v.x + S.x) / 2, w = c.y + (v.y + S.y) / 2, H = Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2)), M = Math.sqrt(Math.pow(S.x, 2) + Math.pow(S.y, 2));
      } else {
        switch (e) {
          case "tl":
            p = r + s.x, a = d + s.y, c = l, f = n, g = o;
            break;
          case "tr":
            p = r + o.x, a = d + o.y, c = n, f = l, g = s;
            break;
          case "bl":
            p = r + n.x, a = d + n.y, c = o, f = s, g = l;
            break;
          case "br":
            p = r + l.x, a = d + l.y, c = s, f = o, g = n;
            break;
        }
        y = { x: p - c.x, y: a - c.y }, m = { x: f.x - c.x, y: f.y - c.y }, N = { x: g.x - c.x, y: g.y - c.y }, W = (y.x * m.x + y.y * m.y) / (Math.pow(m.x, 2) + Math.pow(m.y, 2)), T = (y.x * N.x + y.y * N.y) / (Math.pow(N.x, 2) + Math.pow(N.y, 2)), v = { x: m.x * W, y: m.y * W }, S = { x: N.x * T, y: N.y * T }, b = c.x + (v.x + S.x) / 2, w = c.y + (v.y + S.y) / 2, H = Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2)), M = Math.sqrt(Math.pow(S.x, 2) + Math.pow(S.y, 2));
      }
      this.left = b - H / 2, this.top = w - M / 2, H = R(H, this.minW || 0, this.maxW), M = R(M, this.minH || 0, this.maxH), this.parent && (H = R(H, 0, this.parentWidth), M = R(M, 0, this.parentHeight)), this.lockAspectRatio && (console.log(this.lockAspectRatio, this.aspectFactor), H / M > this.aspectFactor ? H = M * this.aspectFactor : M = H / this.aspectFactor), this.width = H, this.height = M, z(this, "resizing", this.left, this.top, this.width, this.height);
    },
    changeWidth(t) {
      const [e] = X(this.grid, t, 0, this.scale), i = this.parentWidth - e - this.left;
      let s = this.bottom;
      this.lockAspectRatio && (s = this.bottom - (this.right - i) / this.aspectFactor);
      const o = tt(this.parentWidth, this.left, i), n = et(this.parentHeight, this.top, s);
      this.right = i, this.bottom = s, this.width = o, this.height = n;
    },
    changeHeight(t) {
      const [, e] = X(this.grid, 0, t, this.scale), i = this.parentHeight - e - this.top;
      let s = this.right;
      this.lockAspectRatio && (s = this.right - (this.bottom - i) * this.aspectFactor);
      const o = tt(this.parentWidth, this.left, s), n = et(this.parentHeight, this.top, i);
      this.right = s, this.bottom = i, this.width = o, this.height = n;
    },
    // 从控制柄松开
    async handleUp(t) {
      this.handle = null;
      const e = new Array(3).fill({
        display: !1,
        position: "",
        origin: "",
        lineLength: ""
      }), i = { vLine: [], hLine: [] };
      for (const n in i)
        i[n] = JSON.parse(JSON.stringify(e));
      const { x: s, y: o } = this.getMouseCoordinate(t);
      this.lastMouseX = s, this.lastMouseY = o, this.resizing && (this.resizing = !1, await this.conflictCheck(), z(this, "refLineParams", i), z(this, "resizestop", this.left, this.top, this.width, this.height)), this.dragging && (this.dragging = !1, await this.conflictCheck(), z(this, "refLineParams", i), z(this, "dragstop", this.left, this.top)), this.rotating && (this.rotating = !1, z(this, "rotatestop", this.rotate)), this.resetBoundsAndMouseState(), C(document.documentElement, k.move, this.move);
    },
    // 新增方法 ↓↓↓
    // 设置属性
    settingAttribute() {
      this.$el.setAttribute("data-is-check", `${this.isConflictCheck}`), this.$el.setAttribute("data-is-snap", `${this.snap}`);
    },
    // 冲突检测
    conflictCheck() {
      const t = this.top, e = this.left, i = this.width, s = this.height;
      if (this.isConflictCheck) {
        const o = this.$el.parentNode.childNodes;
        for (const n of o)
          if (n.className !== void 0 && !n.className.split(" ").includes(this.classNameActive) && n.getAttribute("data-is-check") !== null && n.getAttribute("data-is-check") !== "false") {
            const l = n.offsetWidth, u = n.offsetHeight, [h, r] = this.formatTransformVal(n.style.transform), d = t >= r && e >= h && r + u > t && h + l > e || t <= r && e < h && t + s > r && e + i > h, p = e <= h && t >= r && e + i > h && t < r + u || t < r && e > h && t + s > r && e < h + l, a = t <= r && e >= h && t + s > r && e < h + l || t >= r && e <= h && t < r + u && e > h + l, x = t <= r && e >= h && t + s > r && e < h + l || t >= r && e <= h && t < r + u && e > h + l, W = e >= h && t >= r && e < h + l && t < r + u || t > r && e <= h && e + i > h && t < r + u, T = t <= r && e >= h && t + s > r && e < h + l || t >= r && e <= h && t < r + u && e + i > h;
            (d || p || a || x || W || T) && (this.top = this.mouseClickPosition.top, this.left = this.mouseClickPosition.left, this.width = this.mouseClickPosition.width, this.height = this.mouseClickPosition.height);
          }
      }
    },
    // 检测对齐元素
    async snapCheck() {
      if (this.snap) {
        let t = this.width, e = this.height, i = this.left, s = this.left + t, o = this.top, n = this.top + e;
        const l = new Array(3).fill({
          display: !1,
          position: "",
          origin: "",
          lineLength: ""
        }), u = { vLine: [], hLine: [] };
        for (const T in u)
          u[T] = JSON.parse(JSON.stringify(l));
        const h = {
          value: { x: [[], [], []], y: [[], [], []] },
          display: [],
          position: []
        }, r = this.$el.parentNode.childNodes, { groupWidth: d, groupHeight: p, groupLeft: a, groupTop: x, bln: W } = await this.getActiveAll(r);
        W || (t = d, e = p, i = a, s = a + d, o = x, n = x + p);
        for (const T of r)
          if (T.className !== void 0 && !T.className.split(" ").includes(this.classNameActive) && T.getAttribute("data-is-snap") !== null && T.getAttribute("data-is-snap") !== "false") {
            const [b, w, H] = this.formatTransformVal(T.style.transform);
            if ((H - this.rotate) % 90 === 0) {
              const M = T.offsetWidth, c = T.offsetHeight, f = b + M, g = w + c, y = Math.abs(o + e / 2 - (w + c / 2)) <= this.snapTolerance, m = Math.abs(i + t / 2 - (b + M / 2)) <= this.snapTolerance, N = Math.abs(w - n) <= this.snapTolerance, v = Math.abs(g - n) <= this.snapTolerance, S = Math.abs(w - o) <= this.snapTolerance, I = Math.abs(g - o) <= this.snapTolerance, U = Math.abs(b - s) <= this.snapTolerance, _ = Math.abs(f - s) <= this.snapTolerance, O = Math.abs(b - i) <= this.snapTolerance, q = Math.abs(f - i) <= this.snapTolerance;
              h.display = [N, v, S, I, y, y, U, _, O, q, m, m], h.position = [w, g, w, g, w + c / 2, w + c / 2, b, f, b, f, b + M / 2, b + M / 2], W && (N && (this.top = w - e, this.bottom = this.parentHeight - this.top - e, h.value.y[0].push(b, f, i, s)), S && (this.top = w, this.bottom = this.parentHeight - this.top - e, h.value.y[0].push(b, f, i, s)), v && (this.top = g - e, this.bottom = this.parentHeight - this.top - e, h.value.y[1].push(b, f, i, s)), I && (this.top = g, this.bottom = this.parentHeight - this.top - e, h.value.y[1].push(b, f, i, s)), U && (this.left = b - t, this.right = this.parentWidth - this.left - t, h.value.x[0].push(w, g, o, n)), O && (this.left = b, this.right = this.parentWidth - this.left - t, h.value.x[0].push(w, g, o, n)), _ && (this.left = f - t, this.right = this.parentWidth - this.left - t, h.value.x[1].push(w, g, o, n)), q && (this.left = f, this.right = this.parentWidth - this.left - t, h.value.x[1].push(w, g, o, n)), y && (this.top = w + c / 2 - e / 2, this.bottom = this.parentHeight - this.top - e, h.value.y[2].push(b, f, i, s)), m && (this.left = b + M / 2 - t / 2, this.right = this.parentWidth - this.left - t, h.value.x[2].push(w, g, o, n)), this.snapBorder && (Math.abs(this.left - 0) <= this.snapTolerance && (this.left = 0, this.right = this.parentWidth - this.left - t), Math.abs(this.right - 0) <= this.snapTolerance && (this.right = 0, this.left = this.parentWidth - this.width - this.right), Math.abs(this.top - 0) <= this.snapTolerance && (this.top = 0, this.bottom = this.parentHeight - this.top - e), Math.abs(this.bottom - 0) <= this.snapTolerance && (this.bottom = 0, this.top = this.parentHeight - this.bottom - e)));
              let B = this.bounds;
              this.left = R(this.left, B.minLeft, B.maxLeft), this.top = R(this.top, B.minTop, B.maxTop), this.right = R(this.right, B.minRight, B.maxRight), this.bottom = R(this.bottom, B.minBottom, B.maxBottom);
              const P = [0, 1, 0, 1, 2, 2, 0, 1, 0, 1, 2, 2];
              for (let L = 0; L <= P.length; L++) {
                const ht = L < 6 ? "y" : "x", E = L < 6 ? "hLine" : "vLine";
                if (h.display[L]) {
                  const { origin: ot, length: nt } = this.calcLineValues(h.value[ht][P[L]]);
                  u[E][P[L]].display = h.display[L], u[E][P[L]].position = h.position[L] + "px", u[E][P[L]].origin = ot, u[E][P[L]].lineLength = nt;
                }
              }
            }
          }
        z(this, "refLineParams", u);
      }
    },
    // 计算参考线
    calcLineValues(t) {
      const e = Math.max(...t) - Math.min(...t) + "px", i = Math.min(...t) + "px";
      return { length: e, origin: i };
    },
    async getActiveAll(t) {
      const e = [], i = [], s = [];
      let o = 0, n = 0, l = 0, u = 0;
      for (const d of t)
        d.className !== void 0 && d.className.split(" ").includes(this.classNameActive) && e.push(d);
      const h = e.length;
      if (h > 1) {
        for (const d of e) {
          const p = d.offsetLeft, a = p + d.offsetWidth, x = d.offsetTop, W = x + d.offsetHeight;
          i.push(p, a), s.push(x, W);
        }
        o = Math.max(...i) - Math.min(...i), n = Math.max(...s) - Math.min(...s), l = Math.min(...i), u = Math.min(...s);
      }
      return { groupWidth: o, groupHeight: n, groupLeft: l, groupTop: u, bln: h === 1 };
    },
    // 修复 正则获取left与top  string.match(/[\d|\.]+/g)
    formatTransformVal(t) {
      let [e, i, s = 0] = t.match(/[\d|.]+/g);
      return i === void 0 && (i = 0), [Number(e), Number(i), s];
    }
  },
  emits: [
    "update:active",
    "rotating",
    "dragging",
    "resizing",
    "refLineParams",
    "resizestop",
    "dragstop",
    "rotatestop",
    "activated",
    "deactivated"
  ]
}, gt = ["onMousedown", "onTouchstart"];
function yt(t, e, i, s, o, n) {
  return F(), $("div", {
    style: j(n.style),
    class: J([
      {
        [i.classNameActive]: t.enabled,
        [i.classNameDragging]: t.dragging,
        [i.classNameResizing]: t.resizing,
        [i.classNameDraggable]: i.draggable,
        [i.classNameResizable]: i.resizable,
        [i.classNameRotating]: t.rotating,
        [i.classNameRotatable]: i.rotatable
      },
      i.className
    ]),
    onMousedown: e[0] || (e[0] = (...l) => n.elementMouseDown && n.elementMouseDown(...l)),
    onTouchstart: e[1] || (e[1] = (...l) => n.elementTouchDown && n.elementTouchDown(...l))
  }, [
    (F(!0), $(at, null, rt(n.actualHandles, (l, u) => (F(), $("div", {
      key: u,
      class: J([i.classNameHandle, i.classNameHandle + "-" + l]),
      style: j(n.handleStyle(l, u)),
      onMousedown: G((h) => n.handleDown(l, h), ["stop", "prevent"]),
      onTouchstart: G((h) => n.handleTouchDown(l, h), ["stop", "prevent"])
    }, [
      K(t.$slots, l, {}, void 0, !0)
    ], 46, gt))), 128)),
    K(t.$slots, "default", {}, void 0, !0)
  ], 38);
}
const st = /* @__PURE__ */ dt(ft, [["render", yt], ["__scopeId", "data-v-e57e5d18"]]);
function V(t) {
  V.installed || (V.installed = !0, t.component("VueDragResizeRotate", st));
}
st.install = V;
export {
  st as default
};
