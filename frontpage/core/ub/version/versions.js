(function () {
    class Updater {
        version = 0;
        constructor(fromVersion) {
            this.version = fromVersion;
        }
        async update() { }
    }
    class Version {
        /**
         * @type Updater[]
         */
        updaters = [];
        async updateVersion() {
            let projectVersion = 0;
            let p = new Promise((resolve, reject) => {
                OpenBlock.VFS.partition.config.get('project.json', p => {
                    if (p) {
                        if (typeof (p.version) == 'number') {
                            projectVersion = p.version;
                        }
                    }
                    resolve();
                    return;
                });
            });
            await p;
            for (let updater of this.updaters) {
                if (updater.version >= projectVersion) {
                    await updater.update();
                    projectVersion = updater.version;
                }
            }
        }

        addUpdater(updater) {
            this.updaters.push(updater);
            this.updaters.sort((a, b) => a.version - b.version);

        }
    }
    OpenBlock.Version = new Version();
    OpenBlock.VersionUpdater = Updater;
})();